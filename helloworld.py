print("Hello World!")
print('"How are you?" someone said')
# Lainausmerkit voidaan escapeta \ merkillä merkkijonon sisällä, jolloin
# se voidaan tulostaa

# print-funktion valinnaiset parametrit
# sep - merkkijonojen erotusmerkki
# end - merkkijonon lopetusmerkki"
print("\"How are you?\" someone said", end=" ")

print("text", "Other text", sep=";", end="\n \n")

# input-funktio lukee käyttäjän syötteen
name = input("Please insert your name: ")

# muutetaan printissä nimen alkukirjain isoksi kommennolla capitalize()
# name = name.capitalize()

if len(name) > 0 and name[0].isupper() == False:
    name = name[0].upper() + name[1:]

print("Hello " + name + "!")
print("Hello " + name.title() + "!")



# python on vahvasti tyypitetty, string tyyppiin ei voi lisätä int tyyppistä
# muuttujaa
sum = name + str(1)
print(sum)

