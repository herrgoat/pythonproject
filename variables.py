# int - kokonaisluku
# float - liukuluku
# complex - kompleksiluku
# boolean - totuusarvo
# string - merkkijono
# object - oliot

int1 = 1
int2 = 5

#operaattorit +, -, *, /, //, modulo (jakojäännös)

sum = int1 + int2
print(sum)

subtraction = int1 - int2
print(subtraction)

multiplication = int1 * int2
print(multiplication)

division = int1 / int2
print(division)

division2 = int1 // int2
print(division2)

modulo = int1 % int2
print(modulo) 

print("\n")

result = 0
# result = result + 1
result += int1
print(result)

result -= int2
print(result)

result *= int1
print(result)

result /= int2
print(result)

result //= int2
print(result)
